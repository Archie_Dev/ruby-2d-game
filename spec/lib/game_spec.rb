#!/usr/bin/env ruby
#
# game_spec.rb
# Copyright (C) 2020 Francisco Paradella <franciscoleite.dev@protonmail.com>
#
# Distributed under terms of the MIT license.
#

require 'spec_helper'

describe "Game" do
  describe "name" do 
    it "returns a default name of 'Game'" do
      expect(Game.new.name).to eq 'Gosu'
    end
  end
end

